//function to get all the key values of the object
const isObject = require('./checkobject.cjs');
function getKeys(testObj) {
    key = []
    if (isObject(testObj)) {
        findkey = Object.keys;// generate a function which is used to get the keys of object
        key = findkey(testObj);//Now findkey is worked as a function
        return key;
    }
    return [];
}
module.exports = getKeys;