const doMap = require('../mapObject.cjs');
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
console.log(doMap(testObject, function (val) {
    return val;
}));
console.log(doMap({ 'one': 1, 'two': 2, 'three': 3 }, function (val) {
    return val * 3;
}));