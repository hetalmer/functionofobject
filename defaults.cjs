//Function defaults fill the undefined properties that matches in defaultprops as a argument
const isObject = require('./checkobject.cjs');
const getKeys = require('./keys.cjs');
function doDefault(testObject, defaultProps) {
    if (isObject(testObject)) {
        const keyMain = getKeys(testObject);
        const keyProps = getKeys(defaultProps);
        for (let i = 0; i < keyProps.length; i++) {
            if (!keyMain.includes(keyProps[i])) {// check that defaultprops key present in main object or not
                testObject[keyProps[i]] = defaultProps[keyProps[i]];// if not the key and value added to it
            }
        }
        return testObject;
    }
    return []
}
module.exports = doDefault;