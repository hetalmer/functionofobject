//Function invert, as its name it inverts the key and value pair
const isObject = require('./checkobject.cjs');
const getKeys = require('./keys.cjs');
const getValues = require('./values.cjs');
function doInvert(testObject) {
    if (isObject(testObject)) {
        const key = getKeys(testObject);
        const value = getValues(testObject);
        let pair = {};
        for (let i = 0; i < key.length; i++) {
            pair[value[i]] = key[i];// create a pair with invert i.e value:key
        }
        return pair;
    }
    return []
}
module.exports = doInvert;