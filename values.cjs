//function to get all the values of the object which is passed as a argument
const isObject = require("./checkobject.cjs");
const getKeys = require("./keys.cjs");
function getValues(testObj) {
    if (isObject(testObj)) {
        const keys = getKeys(testObj);//get all keys with using key function
        let values = []
        for (let i = 0; i < keys.length; i++) {
            values.push(testObj[keys[i]]); // push the value of every key in values variable
        }
        return values;
    }
    return [];
}
module.exports = getValues;