//Function invert, as its name it inverts the key and value pair
const isObject = require('./checkobject.cjs');
const getKeys = require('./keys.cjs');
const getValues = require('./values.cjs');
function doMap(testObject, callback) {
    if (isObject(testObject) && typeof (callback) === 'function') {
        const key = getKeys(testObject);
        newObj = [];
        for (let i = 0; i < key.length; i++) {
            newObj[key[i]] = callback(testObject[key[i]]);// call callback function with the key value
        }
        return newObj;
    }
    return [];
}
module.exports = doMap;