//Function to get all the values of object in pair of array with key and value
const isObject = require('./checkobject.cjs');
const getKeys = require('./keys.cjs');
const getValues = require('./values.cjs');
function getPairs(testObject) {
    if (isObject(testObject)) {
        const key = getKeys(testObject);
        const value = getValues(testObject);
        let pair = [];
        for (let i = 0; i < key.length; i++) {
            pair[i] = [key[i], value[i]];// create a pair with key and value
        }
        return pair;
    }
    return []
}
module.exports = getPairs;