function isObject(testObj) {
    if (typeof (testObj) === 'function' || (typeof (testObj) === 'object' && !!testObj)) {
        return true;
    }
    else {
        return false;
    }
}
module.exports = isObject;